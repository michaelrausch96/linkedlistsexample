#pragma once
class linkedlist
{
public:
private:
	typedef struct node{
		int data;
		node* next;
	}*nodePtr;

	nodePtr head;
	nodePtr curr;
	nodePtr temp;

public:
	linkedlist();
	void AddNode(int addData);
	void DeleteNode(int delData);
	void PrintList();
};

